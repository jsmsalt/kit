<?php

namespace Kit\App\Controllers;

class Example extends \Kit\Core\Controller
{
	function index($request, $response)
	{

		$userValidator = new \Kit\App\Validators\User(['name' => 'Jokkknn', 'email' => 'emai@lom.cc']);

		var_dump($userValidator->validate());
		var_dump($userValidator->errors());


		// Database Result Cache
		// $item = $this->cache->getItem('test');
		// if(!$item->isHit()) {
		// 	$item->set(['uno' => 1, 'dos' => 2])->expiresAfter(5);
		// 	$this->cache->save($item);
		// }
		// var_dump($item->get());


		// Error handler example
		// return $this->notFound($response);
		// return $this->notAllowed($response);


		// Redirect
		// return $this->redirectTo($response, 'route_name');
		// return $this->redirect($response, 'http://...');
		

		// View
		// return $this->render($response, 'template', ['some' => 'data']);
		// $html = $this->fetch('template', ['some' => 'data']);
		// $this->view['data'] = 'some_data_to_view';
		

		// Logger
		// $this->logger->debug('example log');
		// $this->logger->info('example log');
		// $this->logger->notice('example log');
		// $this->logger->warning('example log');
		// $this->logger->error('example log');
		// $this->logger->critical('example log');
		// $this->logger->alert('example log');
		// $this->logger->emergency('example log');


		// Flash
		// $message = $this->flash->getMessage($key);
		// $messages = $this->flash->getMessages();
		// $this->flash->addMessage($key, $message);


		// Return JSON
		// return $response->withJson(['1' => 'xxxxxxx','2' => 'xxxxxxxxxx','3' => 'xxxxxxxxxxxx']);


		//	Redis
		//	$this->redis->get($key);
		//	$this->redis->set($key, $json, 'ex', 20);


		// Geolocation
		// $geo = new \Kit\Core\Geolocation(ROOT.'core'.DS.'data'.DS.'GeoLite2-City.mmdb');
		// print_r($geo->get('81.7.9.157'));
		// $geo->close();


		// Push
		// $push = new \Kit\Core\Push();
		// $push->send($channel, ['some' => 'data']);


		// Encrypt
		// $public_key = 'any_key';
		// $private_key = null;
		// $data = 'any_data';
		// $encrypt_data = null;
		// 
		// try {
		// 	$private_key_string = \Defuse\Crypto\KeyProtectedByPassword::createRandomPasswordProtectedKey($public_key)->saveToAsciiSafeString();
		// } catch (\Exception $e) {
		// 	return false;
		// }
		// 
		// try {
		// 	$private_key = \Defuse\Crypto\KeyProtectedByPassword::loadFromAsciiSafeString($private_key_string)->unlockKey($public_key);
		// } catch (\Exception $e) {
		// 	return false;
		// }
		// 
		// try {
		// 	$encrypt_data = \Defuse\Crypto\Crypto::encrypt($data, $private_key);
		// 	var_dump($encrypt_data);
		// } catch (\Exception $e) {
		// 	return false;
		// }
		// 
		// try {
		// 	$data = \Defuse\Crypto\Crypto::decrypt($encrypt_data, $private_key);
		// 	var_dump($data);
		// } catch (\Exception $e) {
		// 	return false;
		// }



	}
}