<?php

// Example
$app->get('/example', 'Kit\App\Controllers\Example:index')->setName('example')->add($container->csrf);

// Example group
// $app->group('/user', function () use ($app) {
// 	$app->get('/register', 'Kit\App\Controllers\User:register')->setName('user_register');
// 	$app->post('/register', 'Kit\App\Controllers\User:register_post')->setName('user_register_post');
// });