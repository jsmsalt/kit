<?php

return [
	'settings' => [
		'displayErrorDetails' => true,
		'debug' => true,
		'log' => true,
		'determineRouteBeforeAppMiddleware' => true,
		'addContentLengthHeader' => false,		
		'view' => [
			'twig' => [
				'cache' => false,
				'debug' => true,
			],
		],
		// 'db' => [
		// 	'default' => [
		// 		'driver' => 'mysql',
		// 		'host' => 'localhost',
		// 		'database' => 'name',
		// 		'username' => 'user',
		// 		'password' => 'password',
		// 		'charset' => 'utf8',
		// 		'collation' => 'utf8_unicode_ci',
		// 		'prefix' => '',
		// 	],
		// ],
	],
];
