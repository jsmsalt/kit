'use strict';

$(function () {

	var notify = function (message) {
		UIkit.notify(message, {status:'info', timeout:2000, pos:'bottom-right'});
	};

	var app = new Vue({
		delimiters: ['${', '}'],
		el: '#app',
		data: {
			message: null,
			texto: 'jaja',
			posts: null,
			socket: null
		},
		created: function () {

var connection = new autobahn.Connection({url: 'ws://127.0.0.1:8080',realm: 'com.kit.app',max_retries: 5,max_retry_delay: 60});

connection.onopen = function (session) {
	console.debug('Connection open.');

    session.subscribe('com.notifications.general', function (data) {
    	notify(data[0].msg);
    });
};

connection.onclose = function (reason) {
	console.debug('Connection closed.');
};

connection.open();



			this.loadData();
		},
		methods: {
			loadData: function () {
				this.$http.get('/test').then((response) => {
					if (response.ok) {
						this.posts = response.body;
					}
				});
			},
			send: function () {
				this.socket.send(this.message);
				this.message = null;
			}
		}
	})
});