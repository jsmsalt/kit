// PACKAGES
// *******************************************************
var pkg = require('./package.json');
var gulp = require("gulp");
var notify = require("gulp-notify");
var clean = require("gulp-clean");
var del = require('del');
var copy = require('gulp-copy');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var less = require('gulp-less');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ignore = require('gulp-ignore');
var minify = require('gulp-minify-css');
var merge = require('merge-stream');
var sync = require('browser-sync').create();
var favicons = require("gulp-favicons");


var notifyConfig = {
			title: "Success",
			message: "Build with success! File '<%= file.relative %>'",
			wait: false
		};


// FOLDERS & FILES
// *******************************************************

var js = '../public/assets/js/';
var css = '../public/assets/css/';
var fonts = '../public/assets/fonts/';
var images = '../public/assets/images/';

var fonts_files = ['./bower_components/uikit/fonts/**/*'];

var js_front_files = [
	'./bower_components/vue/dist/vue.min.js',
	'./bower_components/axios/dist/axios.min.js',
	'./bower_components/uikit/js/uikit.min.js',
	'./bower_components/uikit/js/components/sticky.js',
	'./bower_components/uikit/js/components/notify.js',
	'./bower_components/uikit/js/components/upload.js',
	'./bower_components/autobahnjs/autobahn.min.js',
	'./assets/js/front/main.js'
];


var js_admin_files = [
	'./bower_components/uikit/js/uikit.min.js',
	'./assets/js/admin/main.js'
];

var scss_front_files = ['./assets/css/front/include.scss'];
var less_front_files = [];
var css_front_files = [];

var scss_admin_files = ['./assets/css/admin/include.scss'];
var less_admin_files = [];
var css_admin_files = [];



// TASKS
// *******************************************************

gulp.task('clean', function() {
	return del([css+'*', js+'*', fonts+'*'], {force: true});
});

gulp.task('copy:fonts', function() {
	return gulp.src(fonts_files)
		.pipe(copy(fonts, {prefix: 3}));
});

gulp.task('copy:vendors', function() {
	return gulp.src([
			'./bower_components/jquery/dist/jquery.min.js',
			'./bower_components/html5shiv/dist/html5shiv.min.js'
		])
		.pipe(copy(js, {prefix: 3}));
});

gulp.task("favicons", function () {
	return gulp.src("./assets/images/favicon.png")
		.pipe(favicons({
			appName: pkg.name,
			appDescription: pkg.description,
			developerName: pkg.author,
			background: "#FFFFFF",
			path: "/assets/images/favicons/",
			display: "standalone",
			orientation: "portrait",
			start_url: "/",
			version: 1.0,
			logging: true,
			online: false,
			html: "./../../../../app/Views/partials/favicons.twig",
			pipeHTML: true,
			replace: true,
			icons: {
				android: true,
				appleIcon: true,
				appleStartup: true,
				coast: { offset: 25 },
				favicons: true,
				firefox: false,
				windows: false,
				yandex: false
			}
		}))
		.pipe(gulp.dest("./../public/assets/images/favicons/"));
});

gulp.task('js:front', function() {
	return gulp.src(js_front_files)
		.pipe(babel({presets: ['es2015'],compact: false,only: ['./assets/js/front/main.js']}))
		.pipe(concat('main.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(js))
		.pipe(sync.stream())
		.pipe(notify(notifyConfig));
});

gulp.task('js:admin', function() {
	return gulp.src(js_admin_files)
		.pipe(babel({presets: ['es2015'], compact: false,only: ['./assets/js/admin/main.js']}))
		.pipe(concat('admin.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(js))
		.pipe(sync.stream())
		.pipe(notify(notifyConfig));
});

gulp.task('css:front', function() {
	var lessStream = gulp.src(less_front_files)
		.pipe(less());
	var scssStream = gulp.src(scss_front_files)
		.pipe(sass({
			outputStyle: 'compressed',
			sourceMap: false,
			includePaths: ['./bower_components/uikit/scss', './assets/css/front']
		}).on('error', sass.logError));
	var cssStream = gulp.src(css_front_files);

	return merge(lessStream, scssStream, cssStream)
		.pipe(concat('main.min.css'))
		.pipe(autoprefixer('last 10 version'))
		.pipe(minify())
		.pipe(gulp.dest(css))
		.pipe(sync.stream())
		.pipe(notify(notifyConfig));
});

gulp.task('css:admin', function() {
	var lessStream = gulp.src(less_admin_files)
		.pipe(less());
	var scssStream = gulp.src(scss_admin_files)
		.pipe(sass({
			outputStyle: 'compressed',
			sourceMap: false,
			includePaths: ['./bower_components/uikit/scss', './assets/css/admin']
		}).on('error', sass.logError));
	var cssStream = gulp.src(css_admin_files);

	return merge(lessStream, scssStream, cssStream)
		.pipe(concat('admin.min.css'))
		.pipe(autoprefixer('last 10 version'))
		.pipe(minify())
		.pipe(gulp.dest(css))
		.pipe(sync.stream())
		.pipe(notify(notifyConfig));
});
 
gulp.task('watch', function () {
	var watch_front_css = scss_front_files
							.concat(less_front_files)
							.concat(css_front_files);
	watch_front_css.push('./assets/css/front/*.scss');

	var watch_admin_css = scss_admin_files
							.concat(less_admin_files)
							.concat(css_admin_files);
	watch_admin_css.push('./assets/css/admin/*.scss');

	sync.init({
		host: 'cajas.dev',
		proxy: 'cajas.dev',
		port: 3000
	});

	gulp.watch(js_front_files, ['js:front']);
	gulp.watch(js_admin_files, ['js:admin']);
	gulp.watch(watch_front_css, ['css:front']);
	gulp.watch(watch_admin_css, ['css:admin']);
	gulp.watch('./../app/Views/**/*.twig').on('change', sync.reload);
});

gulp.task('default', ['clean','copy:fonts','copy:vendors','js:front','js:admin','css:front','css:admin', 'watch'], function() {});
gulp.task('compile', ['clean','copy:fonts','copy:vendors','js:front','js:admin','css:front','css:admin'], function() {});
