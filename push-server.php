<?php
// Path constants
define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT', dirname(__FILE__).DS );
require ROOT.'core'.DS.'vendor'.DS.'autoload.php';

use \React\EventLoop\Factory;
use \React\ZMQ\Context;
use \Thruway\Peer\Client;
use \Thruway\Peer\Router;
use \Thruway\Transport\RatchetTransportProvider;

$loop   = Factory::create();
$pusher = new Client('com.kit.app', $loop);

$pusher->on('open', function ($session) use ($loop) {
    $context = new Context($loop);
    $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
    $pull->bind('tcp://127.0.0.1:5555');
    $pull->on('message', function ($data) use ($session) {
        $sendData = json_decode($data, true);
        if (!empty($sendData['topic']) && is_array($sendData['data'])) {
        	$session->publish($sendData['topic'], [$sendData['data']]);
        }
    });
});

$router = new Router($loop);
$router->addInternalClient($pusher);
$router->addTransportProvider(new RatchetTransportProvider('0.0.0.0', 8080));
$router->start();