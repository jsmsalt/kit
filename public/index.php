<?php

// Request for static content check
if (PHP_SAPI == 'cli-server') {
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

// Path constants
define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT', dirname(__FILE__).DS.'..'.DS );

// Require boostrap
require ROOT.'core'.DS.'bootstrap.php';