<?php

$core_settings = require ROOT.'core'.DS.'settings.php';
$app_settings = file_exists(ROOT.'app'.DS.'settings.php') ? require ROOT.'app'.DS.'settings.php':[];
$settings = array_replace_recursive($core_settings, $app_settings);

// Session
session_cache_limiter(false);
session_start();

// Error report
ini_set('error_reporting', '1');
ini_set('display_errors', 'On');
ini_set('display_startup_errors', 'On');

// Locale setting
setlocale(LC_ALL, 'es_ES');
date_default_timezone_set("UTC");

// Require Composer autoload
require ROOT.'core'.DS.'vendor'.DS.'autoload.php';

// Create app
$app = new \Slim\App($settings);

// Setup dependencies
require ROOT.'core'.DS.'dependencies.php';
if(file_exists(ROOT.'app'.DS.'dependencies.php'))
	require ROOT.'app'.DS.'dependencies.php';

// Register middleware
require ROOT.'core'.DS.'middleware.php';
if(file_exists(ROOT.'app'.DS.'middleware.php'))
	require ROOT.'app'.DS.'middleware.php';

// Register routes
if(file_exists(ROOT.'app'.DS.'routes.php'))
	require ROOT.'app'.DS.'routes.php';

// Run
$app->run();