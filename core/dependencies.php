<?php

$container = $app->getContainer();

// Bootstrap Eloquent
if(isset($container->settings['db'])) {
	$capsule = new \Illuminate\Database\Capsule\Manager;

	foreach ($container->settings['db'] as $name => $config) {
		$capsule->addConnection($config, $name);
	}

	$capsule->setAsGlobal();
	$capsule->bootEloquent();

	$container['db'] = function ($container) use ($capsule) {
		return $capsule;
	};
}

// CSRF
$container['csrf'] = function ($container) {
	return new \Slim\Csrf\Guard;
};

// Twig
$container['view'] = function ($container) {
	$view = new Slim\Views\Twig($container->settings['view']['template_path'], $container->settings['view']['twig']);

	// Add extensions
	$view->addExtension(new Slim\Views\TwigExtension($container->router, $container->request->getUri()));
	$view->addExtension(new Twig_Extension_Debug());
	$view->addExtension(new Kit\Core\Twig\CsrfExtension($container->csrf));
	return $view;
};

// Flash messages
$container['flash'] = function ($container) {
	return new Slim\Flash\Messages;
};

// Cache
$container['cache'] = function ($container) {
	return \phpFastCache\CacheManager::Redis($container->settings['redis']);
};

// Monolog
$container['logger'] = function ($container) {
	$logger = new \Monolog\Logger($container->settings['logger']['name']);
	$logger->pushProcessor(new Monolog\Processor\UidProcessor());
	$logger->pushHandler(new Monolog\Handler\StreamHandler($container->settings['logger']['path'], Monolog\Logger::DEBUG));
	return $logger;
};

// Input
$container['input'] = function ($container) {
	return new \Kit\Core\Input;
};

// Cookie
$container['cookie'] = function ($container) {
	return new \Kit\Core\Cookie;
};

// IP
$container['ip'] = function ($container) {
	return new \Vectorface\Whip\Whip(\Vectorface\Whip\Whip::REMOTE_ADDR);
};

// Core Controller
$container['\Kit\Core\Controller'] = function ($container) {
	return new \Kit\Core\Controller($container);
};

// Core Middleware
$container['\Kit\Core\Middleware'] = function ($container) {
	return new \Kit\Core\Middleware($container);
};

// Error 404 handler
$container['notFoundHandler'] = function ($container) { 
    return function ($request, $response) use ($container) {
        // return $container->view->render($response, 'errors/404.twig')->withStatus(404); 
        $template = $container->view->fetch('errors/404.twig');
        return $response->write($template)->withHeader('Content-Type', 'text/html')->withStatus(404); 
    };
};

// Error 405 handler
$container['notAllowedHandler'] = function ($container) { 
    return function ($request, $response) use ($container) {
        $template = $container->view->fetch('errors/405.twig');
        return $response->write($template)->withHeader('Content-Type', 'text/html')->withStatus(405); 
    };
};

// // Error handler
// $container['errorHandler'] = function ($container) { 
//     return function ($request, $response) use ($container) {
//         $template = $container->view->fetch('errors/500.twig');
//         return $response->write($template)->withHeader('Content-Type', 'text/html')->withStatus(500); 
//     };
// };

// // Error 500 handler
// $container['phpErrorHandler'] = function ($container) { 
//     return function ($request, $response) use ($container) {
//         $template = $container->view->fetch('errors/500.twig');
//         return $response->write($template)->withHeader('Content-Type', 'text/html')->withStatus(500); 
//     };
// };
