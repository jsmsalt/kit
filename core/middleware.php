<?php
$app->add(new \Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware($app));
$app->add('\Kit\Core\Middlewares\TwigData');
$app->add('\Kit\Core\Middlewares\Minify');