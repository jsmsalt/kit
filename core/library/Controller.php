<?php

namespace Kit\Core;
use \Kit\Core\Cookie;
use \Kit\Core\Input;

abstract class Controller
{
	protected $container;
	protected $page_title;
	protected $page_id;
	protected $page_type;
	protected $page_date;
	protected $page_robots;
	protected $page_image;
	protected $page_description;
	protected $page_author;
	protected $page_keywords;
	protected $page_revisit;
	protected $page_expires;

	public function __construct($container)
	{
		$this->container = $container;
	}

	public function __get($property)
	{
		if ($this->container->{$property}) {
			return $this->container->{$property};
		}
	}

	public function redirectTo($response, $name)
	{
		$url = $this->router->pathFor($name);
		return $this->redirect($response, $url);
	}

	public function redirect($response, $url)
	{
		return $response->withStatus(302)->withHeader('Location', $url);
	}

	public function notFound($response)
	{
		$notFoundHandler = $this->container['notFoundHandler'];
		return $notFoundHandler(null, $response);
	}

	public function notAllowed($response)
	{
		$notAllowedHandler = $this->container['notAllowedHandler'];
		return $notAllowedHandler(null, $response);
	}

	public function serverError($request, $response)
	{
		$phpErrorHandler = $this->container['phpErrorHandler'];
		return $phpErrorHandler($request, $response);
	}

	public function render($response, $template, $data = [])
	{
		$this->_page_data();
		return $this->view->render($response, $template.'.twig', $data);
	}

	public function fetch($template, $data = [])
	{
		$this->_page_data();
		return $this->view->fetch($template.'.twig', $data);
	}

	private function _page_data()
	{
		$page_data = [
			'title' => $this->page_title,
			'date' => $this->page_date,
			'type' => $this->page_type,
			'robots' => $this->page_robots,
			'image' => $this->page_image,
			'author' => $this->page_author,
			'description' => $this->page_description,
			'revisit' => $this->page_revisit,
			'expires' => $this->page_expires,
			'keywords' => $this->page_keywords,
		];

		$page_data = array_filter($page_data);
		$app_data = $this->view['app'];
		$app_data = array_replace_recursive($app_data, $page_data);
		$this->view['app'] = $app_data;
	}

}