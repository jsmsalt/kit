<?php

namespace Kit\Core;

class Push {
	protected $socket = null;

	public function __construct($uri = 'tcp://127.0.0.1:5555', $realm = 'com.kit.app')
	{
		try {
		    $context = new \ZMQContext;
		    $this->socket  = $context->getSocket(\ZMQ::SOCKET_PUSH, $realm);
		    $this->socket->connect($uri);
		    return true;
		}
		catch (\ZMQSocketException $e) {
		    return false;
		}
	}

	public function send($topic, $data)
	{
		$sendData = [
			'topic' => $topic,
			'data' => $data
		];

		try {
		    $this->socket->send(json_encode($sendData));
		    return true;
		}
		catch (\ZMQSocketException $e) {
		    return false;
		}
	}
}


