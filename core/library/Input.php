<?php

namespace Kit\Core;

class Input
{
	public function post($request, $key = null)
	{
		if(is_null($key))
		{
			return $request->getParsedBody();
		}
		else
		{
			$inputs = $request->getParsedBody();

			if(is_array($inputs))
				return array_key_exists($key, $inputs) ? $inputs[$key] : null;
			else
				return null;
		}
	}

	public function get($request, $key = null)
	{
		if(is_null($key))
		{
			return $request->getQueryParams();
		}
		else
		{
			$inputs = $request->getQueryParams();

			if(is_array($inputs))
				return array_key_exists($key, $inputs) ? $inputs[$key] : null;
			else
				return null;
		}
	}
}