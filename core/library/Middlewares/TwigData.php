<?php

namespace Kit\Core\Middlewares;

class TwigData extends \Kit\Core\Middleware
{
	public function __invoke($request, $response, $next)
	{
		$route = $request->getAttribute('route');
		$route_name = is_null($route) ? null:$route->getName();
		$route_base= $request->getUri()->getBaseUrl();
		$route_params = isset($request->getAttribute('routeInfo')[2]) ? $request->getAttribute('routeInfo')[2]: null;
		$route_path = is_null($route_name) || is_null($route_params) ? null:$this->router->pathFor($route_name, $route_params);
		$route_url = is_null($route_path) ? null:$route_base.$route_path;
		$app_data = [
			'id' => $route_name,
			'canonical' => $route_base,
			'path' => $route_path,
			'url' => $route_url,
		];
		$setting_data = array_filter($this->settings['app'] ? $this->settings['app']:[]);
		$data = array_replace_recursive($setting_data, $app_data);
	 	$this->view['app'] = $data;
	 	$this->view['flash'] = $this->flash->getMessages();

		return $next($request, $response);
	}
}