<?php

namespace Kit\Core\Middlewares;

class Minify
{
    public function __invoke($request, $response, $next)
    {
        // call next middleware
        $response = $next($request, $response);
        $content = (string)$response->getBody();

        $search = array(
            // remove tabs before and after HTML tags
            '/\>[^\S ]+/s',
            '/[^\S ]+\</s',
            // remove empty lines (between HTML tags); cannot remove just any line-end characters because in inline JS they can matter!
            '/\>[\r\n\t ]+\</s',
            // shorten multiple whitespace sequences
            '/(\s)+/s',
            // replace end of line by a space
            '/\n/',
            // Remove any HTML comments, except MSIE conditional comments
            '/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s',
        );
        $replace = array(
            '>',
            '<',
            '><',
            '\\1',
            ' ',
            ''
        );
        $newContent = preg_replace($search, $replace, $content);
        $newBody = new \Slim\Http\Body(fopen('php://temp', 'r+'));
        $newBody->write($newContent);
        return $response->withBody($newBody);
    }
}