<?php

namespace Kit\Core;

use \Dflydev\FigCookies\Cookies;
use \Dflydev\FigCookies\SetCookie;
use \Dflydev\FigCookies\FigRequestCookies;
use \Dflydev\FigCookies\FigResponseCookies;

class Cookie
{
	public function set($response, $key, $value, $expire = '+30 minutes', $max_age = false, $path = false, $domain = false, $secure = null, $http = null)
	{
		$date = new \Datetime($expire);
		$cookie = SetCookie::create($key)->withValue($value)->withExpires($date->format(\DateTime::COOKIE));

		if($max_age !== false)
			$cookie = $cookie->withMaxAge($max_age);

		if($path !== false)
			$cookie = $cookie->withPath($path);

		if($domain !== false)
			$cookie = $cookie->withDomain($domain);

		if($secure !== null)
			$cookie = $cookie->withSecure($secure);

		if($http !== null)
			$cookie = $cookie->withHttpOnly($http);

		return FigResponseCookies::set($response, $cookie);
	}

	public function get($request, $key, $default = null)
	{
		$cookie = FigRequestCookies::get($request, $key, $default);
		return is_null($cookie) ? null:$cookie->getValue();
	}

	public function getAll($request)
	{
		$cookies = Cookies::fromRequest($request)->getAll();

		if(is_array($cookies))
		{
			$cookies_array = [];

			foreach ($cookies as $cookie)
			{
				$cookies_array[$cookie->getName()] = $cookie->getValue();
			}

			return $cookies_array;
		}
		else
		{
			return [];
		}
	}

	public function remove($response, $key)
	{
		$response = FigResponseCookies::remove($response, $key);
		$response = FigResponseCookies::expire($response, $key);
		return $response;
	}

}