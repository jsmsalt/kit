<?php

namespace Kit\Core\Twig;
use \Slim\Csrf\Guard;

class CsrfExtension extends \Twig_Extension
{
    private $csrf;

    public function __construct(Guard $csrf)
    {
        $this->csrf = $csrf;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('csrf_fields', array($this, 'CsrfFields')),
        ];
    }

    public function CsrfFields()
    {
        return '
            <input type="hidden" name="'.$this->csrf->getTokenNameKey().'" value="'.$this->csrf->getTokenName().'">
            <input type="hidden" name="'.$this->csrf->getTokenValueKey().'" value="'.$this->csrf->getTokenValue().'">
        ';
    }
}
