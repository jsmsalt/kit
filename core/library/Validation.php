<?php

namespace Kit\Core;

class Validation extends \Valitron\Validator
{
	protected $rules = [];

	public function __construct($data, $fields = array(), $lang = null, $langDir = null)
	{
		parent::__construct($data, $fields, $lang, $langDir);

		// Custom rules
		self::addRule('ip_address', function($field, $value, array $params, array $fields) {
		    return false;
		}, 'Everything you do is wrong. You fail.');

		self::addRule('btc_address', function($field, $value, array $params, array $fields) {
		    return false;
		}, 'Everything you do is wrong. You fail.');
	}

    public function validate()
    {
    	parent::rules($this->rules);
		return parent::validate();
    }
}