<?php

namespace Kit\Core;

class Model extends \Illuminate\Database\Eloquent\Model {
	public $primaryKey = 'id';
	public $timestamps = false;
	public $incrementing = false;
	protected $connection = 'default';
	protected $dateFormat = 'Y-m-d H:i:s';
}


