<?php

return [
	'settings' => [
		// Slim settings
		'determineRouteBeforeAppMiddleware' => true,
		'displayErrorDetails' => false,
		'debug' => false,
		'addContentLengthHeader' => false,

		// Whoops settings
		'whoops' => [
			'editor' => 'sublime'
		],

		// View settings
		'view' => [
			'template_path' => [ROOT.'app'.DS.'Views', ROOT.'core'.DS.'views'],
			'twig' => [
				'cache' => ROOT.'core'.DS.'cache'.DS.'twig',
				'debug' => false,
				'auto_reload' => true,
				'charset' => 'UTF-8',
				'strict_variables' => false,
				'autoescape' => 'html'
			],
		],

		// monolog settings
		'logger' => [
			'name' => 'app',
			'path' => ROOT.'core'.DS.'log'.DS.date('Y-m-d').'.log',
		],

		// Redis caché
		'redis' => [
			'host' => '127.0.0.1',
			'port' => '6379',
			'password' => null,
			'database' => null,
			'timeout' => null
		],

		// Database
		// 'db' => [
		// 	'default' => [
		// 		'driver' => 'mysql',
		// 		'host' => 'localhost',
		// 		'database' => '',
		// 		'username' => 'root',
		// 		'password' => '',
		// 		'charset' => 'utf8',
		// 		'collation' => 'utf8_unicode_ci',
		// 		'prefix' => '',
		// 	],
		// ],
	],
];